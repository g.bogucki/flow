# flow
![](flow_logo.png)

## Opis
Aplikacja powstała po zapoznaniu się z techniką [FLOWMODORO](https://www.thrive-phd.com/blog/2023/4/4/flowmodoro-a-timer-variation-for-deep-focus). 
Skrypt działa w terminalu i zapisuje dane z sesji do pliku *statistic.json*. 

## Wymagania
- Narzędzie jq

### Instalacja jq
**Ubuntu**
`sudo apt install jq`

**Fedora**
`sudo rpm install jq`

**Arch**
`sudo pacman -S jq`

## Uruchomienie
1. Sklonuj repozytorium: `git clone https://gitlab.com/g.bogucki/flow`
2. Przejdź do katalogu projektu: `cd flow`
3. Nadaj uprawnienie dla pliku `chmod +x flow.sh`
4. Uruchom skrypt: `./flow.sh`

### opcjonalnie
Można dodać do katalogu */usr/bin/* bez końcówki `.sh`
```bash
cp flow.sh ~/usr/bin/flow
```
## Opcje
- `-h`, `--help`: Wyświetla pomoc
- `-s`, `--stats`: Wyświetla statystyki 

## TODO

- [x] - `-s`, `--stats`: Wyświetla tylko dane statystyczne
- [ ] - `-p`, `--project <nazwa_projektu>`: Dodaj sesję do konkretnego projektu
- [ ] Wyświetlanie nazwy zadania podczas odliczania
