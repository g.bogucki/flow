#!/bin/bash

COLOR_GREEN='\033[1;32m'
COLOR_YELLOW='\033[0;33m'
COLOR_RED='\033[1;31m'
COLOR_RESET='\033[0m'

# Sprawdź, czy narzędzie 'jq' jest zainstalowane
if ! command -v jq &> /dev/null; then
    echo -e "${COLOR_RED}Błąd: Narzędzie 'jq' nie jest zainstalowane. Proszę zainstaluj 'jq', aby skrypt działał poprawnie.${COLOR_RESET}"
    exit 1
fi

# Pobierz aktualne dane statystyczne z pliku
sessions_count=$(jq '.sessions | length' statistics.json)
total_focus_time=$(jq '[.sessions[].czas_skupienia] | add' statistics.json)
total_break_time=$(jq '[.sessions[].czas_przerwy] | add' statistics.json)

# Ustaw domyślne wartości
show_stats=false

# Funkcja formatująca czas
format_time() {
    local time_in_seconds=$1
    printf "%02d:%02d:%02d" $((time_in_seconds/3600)) $((time_in_seconds%3600/60)) $((time_in_seconds%60))
}

# Funkcja wyświetlająca statystyki
display_statistics() {
    echo -e "${COLOR_YELLOW}Liczba sesji: ${COLOR_GREEN}$sessions_count${COLOR_RESET}"

    formatted_time=$(format_time $total_focus_time)
    echo -e "${COLOR_YELLOW}Łączny czas sesji: ${COLOR_GREEN}$formatted_time${COLOR_RESET}"

    break=$((total_focus_time/5))
    formatted_time=$(format_time $break)

    echo -e "${COLOR_YELLOW}Łączny czas przerw: ${COLOR_RED}$formatted_time${COLOR_RESET}"
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -h|--help)
            # ... (bez zmian)
            exit 0 ;;
        -s|--stats) show_stats=true ;;
        *) echo "Nieznana opcja: $1. Użyj -h/--help, aby uzyskać pomoc." ; exit 1 ;;
    esac
    shift
done

if [ "$show_stats" = true ]; then
    # Wyświetl statystyki
    display_statistics
    exit 0
fi

# Logo
echo -e "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
echo -e "━┏━━━┓━━━━━━━━━━━━━━━━━┏━━━┓┏┓━━━━━━━━━━━"
echo -e "━┃┏━━┛━━━━━━━━━━━━━━━━━┃┏━━┛┃┃━━━━━━━━━━━"
echo -e "━┃┗━━┓┏━━┓┏━━┓┏┓┏┓┏━━┓━┃┗━━┓┃┃━┏━━┓┏┓┏┓┏┓"
echo -e "━┃┏━━┛┃┏┓┃┃┏━┛┃┃┃┃┃━━┫━┃┏━━┛┃┃━┃┏┓┃┃┗┛┗┛┃"
echo -e "┏┛┗┓━━┃┗┛┃┃┗━┓┃┗┛┃┣━━┃┏┛┗┓━━┃┗┓┃┗┛┃┗┓┏┓┏┛"
echo -e "┗━━┛━━┗━━┛┗━━┛┗━━┛┗━━┛┗━━┛━━┗━┛┗━━┛━┗┛┗┛━"
echo -e "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
echo -e "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

sleep 1
clear
# Pytanie o nazwę zadania
read -p $'\e[1;33mPodaj nazwę zadania:\e[0m ' task_name

# Sprawdź, czy plik statistics.json istnieje
if [ ! -e "statistics.json" ]; then
    echo '{"sessions": []}' > statistics.json
fi

# Sprawdź, czy plik 'statistics.json' jest zapisywalny
if [ ! -w "statistics.json" ]; then
    echo -e "${COLOR_RED}Błąd: Brak uprawnień do zapisu do pliku 'statistics.json'. Sprawdź uprawnienia.${COLOR_RESET}"
    exit 1
fi

# Zmienna globalna na dane sesji JSON
session_json=""

# Funkcja formatująca czas
format_time() {
    local time_in_seconds=$1
    printf "%02d:%02d:%02d" $((time_in_seconds/3600)) $((time_in_seconds%3600/60)) $((time_in_seconds%60))
}

# Funkcja wyświetlająca statystyki
display_statistics() {
    echo -e "${COLOR_YELLOW}Liczba sesji: ${COLOR_GREEN}$((sessions_count + 1))${COLOR_RESET}"

    formatted_time=$(format_time $total_focus_time)
    echo -e "${COLOR_YELLOW}Łączny czas sesji: ${COLOR_GREEN}$formatted_time${COLOR_RESET}"

    break=$((total_focus_time/5))
    formatted_time=$(format_time $break)

    echo -e "${COLOR_YELLOW}Łączny czas przerw: ${COLOR_RED}$formatted_time${COLOR_RESET}"
}

# Funkcja odliczająca czas do momentu wciśnięcia Enter
countdown() {
    read -n 1 -s -r -t 1
    clear
    echo -e "\n${COLOR_GREEN}Rozpoczynam odliczanie!${COLOR_RESET}"
    seconds=0
    while true; do
        sleep 1
        ((seconds++))
        formatted_time=$(format_time $seconds)
        echo -ne "Czas: ${COLOR_GREEN}$formatted_time${COLOR_RESET} sekund\r"
        if read -n 1 -s -r -t 0.1; then
            break
        fi
    done
}

# Funkcja odliczająca przerwę
break_countdown() {
    echo -e "\nNaciśnij Enter, aby rozpocząć przerwę..."
    read -n 1 -s -r -t $1
    echo -e "\n${COLOR_RED}Rozpoczynam przerwę!${COLOR_RESET}"
    break_time=$1
    while [ $break_time -gt 0 ]; do
        sleep 1
        ((break_time--))
        formatted_time=$(format_time $break_time)
        echo -ne "Czas przerwy: ${COLOR_RED}$formatted_time${COLOR_RESET} sekund\r"
    done
    echo -e "\nKoniec przerwy!\n"
}

# Funkcja zapisująca statystyki do pliku JSON
save_statistics() {
    local session_number=$1
    local total_focus_time=$2
    local total_break_time=$3

    session_date=$(date +"%Y-%m-%d %H:%M:%S")

    # Stwórz obiekt sesji w formacie JSON
    session_json=$(cat <<EOF
{
  "id": "$session_number",
  "data": "$session_date",
  "name": "$task_name",
  "czas_skupienia": $total_focus_time,
  "czas_przerwy": $((total_focus_time/5))
}
EOF
)

    # Dodaj nową sesję do pliku JSON
    jq --argjson session "$session_json" '.sessions += [$session]' statistics.json > tmpfile && mv tmpfile statistics.json
}

# Główna część skryptu
main() {
    countdown_time=0
    countdown
    break_time=$((seconds / 5))
    if [ $break_time -lt 0 ]; then
        break_time=0
    fi
    break_countdown $break_time

    # Pobierz aktualne dane statystyczne z pliku
    sessions_count=$(jq '.sessions | length' statistics.json)
    total_focus_time=$(jq '[.sessions[]."czas_skupienia"] | add' statistics.json)
    total_break_time=$(jq '[.sessions[]."czas_przerwy"] | add' statistics.json)

    # Zwiększ numer sesji o 1
    session_number=$((sessions_count + 1))

    # Aktualizuj dane statystyczne
    total_focus_time=$((total_focus_time + seconds))
    total_break_time=$((total_break_time + break_time))

    # Zapisz statystyki do pliku JSON
    save_statistics $session_number $seconds $total_break_time

    # Wyświetl statystyki
    display_statistics
}

# Wywołanie funkcji głównej
main

